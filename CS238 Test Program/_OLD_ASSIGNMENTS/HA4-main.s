; File: HA6-main.s
; Author: Brandon Jones

; This file needs to be in a Keil version 5 project, together with file init.s

; This is an initial demo program for HA4, which you need to change to complete HA4

; All future CS 238 Home Assignments, will have similar but different files,
; like HA5-main.s, HA6-main.s, etc.

; Executable code in HAx-main.s files should start at label main

	EXPORT	main		; this line is needed to interface with init.s

; Usable utility functions defined in file init.s
; Importing any label from another source file is necessary
; in order to use that label in this source file

	IMPORT	GetCh		; Input one ASCII character from the UART #1 window (from keyboard)
	IMPORT	PutCh		; Output one ASCII character to the UART #1 window
	IMPORT	PutCRLF		; Output CR and LF to the UART #1 window
    IMPORT	UDivMod		; Perform unsigned division to obtain quotient and remainder
	IMPORT	GetDec		; Input a signed number from the UART #1 window
	IMPORT	PutDec		; Output a signed number to the UART #1 window
	IMPORT	GetStr		; Input a CR-terminated ASCII string from the UART #1 window
	IMPORT	PutStr		; Output null-terminated ASCII string to the UART #1 window
	IMPORT	FindGCD		; Compute the Greatest Common Divisor for two integers
	;IMPORT	PutBin		; Take a Decimal value and print the Binary equilivant

	AREA    MyCode, CODE, READONLY

	ALIGN			; highly recommended to start and end any area with ALIGN

; Start of executable code is at following label: main

main

;-------------------- START OF MODIFIABLE CODE ----------------------

	PUSH	{LR}		; save return address of caller in init.s
	
Repeat
	;Repeat
	LDR R0, =Prompt1
	BL PutStr
	BL GetDec
	;Until
	CMP R0, #0
	BLT Repeat
	CMP R0, #20
	BGT Repeat
	
	LDR R1, =ArraySize ; R1 = Address of ArraySize
	STR R0, [R1]
	
	
	MOV R3, #0
	
	LDR R0, =Prompt2 ; R0 = Address of first byte named Prompt2
	BL PutStr
	
	LDR R2, =Array
	;LDR R4, =Array ; To be used later in the commented out code to print the array.
	
	LDR R1, [R1]
LoopAgain
	;For
	BL GetDec
	STR R0, [R2]
	ADD R2, #4
	ADD R3, #1
	CMP R3, R1
	BLT LoopAgain
	; Values retrieved
	
	
	LDR R1, =ArraySize ; R1 = Address of ArraySize
	LDR R1, [R1]
	
	; ----------------- The Search Begins: -----------------
	
	LDR R0, =Prompt4
	BL PutStr
	BL GetCh

; THIS BREAKS AT SOME POINT.... FIX

While
	;While
	MOV R3, #0 ; R3 = Starting Index 'i'
	BL PutCRLF
	CMP R0, #'Y'
	BEQ SearchAgain
	CMP R0, #'y'
	BNE EndWhile
SearchAgain
	LDR R4, =Array
	LDR R0, =Prompt3
	BL PutStr
	BL GetDec ; R0 = Value to Search For	->	'V'
	LDR R7, =Value
	STR R0, [R7]
	MOV R5, #0 ; Found = False	->	R5 = Found TruthValue (0/1)
LOOPY
	;For
	LDR R6, [R4] ; Value to Compare 'V' (R0) to.
	CMP R0, R6 ; Compare the search value 'V' to the current array value R6
	BNE NotFound
	MOV R5, #1 ; Set our Found TruthValue to 1	->	Found = TRUE
	BAL BREAK
NotFound
	ADD R4, #4 ; Add four bytes (that of an integer) to move address to next decimal value
	ADD R3, #1 ; Increment our counter by 1
	CMP R3, R1 ; Check to see if we have reached the end of the 'Array'
	BLT LOOPY ; If we have not reached the end of our 'Array', then loop again
	;end for
BREAK
	;if
	CMP R5, #1
	BNE NegativeCaptain
	;then
	LDR R0, =ProbeNum
	BL PutStr
	LDR R0, [R7]
	BL PutDec
	LDR R0, =PosResult
	BL PutStr
	B PromptTime
NegativeCaptain
	;else
	LDR R0, =ProbeNum
	BL PutStr
	LDR R0,[R7]
	BL PutDec
	LDR R0, =NegResult
	BL PutStr
	;end if
PromptTime
	LDR R0, =Prompt5
	BL PutStr
	BL GetCh
	BL PutCRLF
	BAL While
EndWhile
	;end while
	
	
	
	; BELOW WILL PRINT OUT EACH ELEMENT IN THE ARRAY (For Debugging)
	
	;LDR R1, =ArraySize ; R1 = Address of ArraySize
	;LDR R1, [R1]
	;;MOV R3, #0
;LOOPY
	;For
	;LDR R0, =ProbeNum
	;BL PutStr
	;LDR R0, [R4]
	;BL PutDec
	;LDR R0, =Value
	;BL PutStr
	;ADD R4, #4
	;ADD R3, #1
	;CMP R3, R1
	;BLT LOOPY
	
	LDR R0, =GoodBye
	BL PutStr

	POP {PC}

; Some commonly used ASCII codes

CR	EQU	0x0D	; Carriage Return (to move cursor to beginning of current line)
LF	EQU	0x0A	; Line Feed (to move cursor to same column of next line)

; The following data items are in the CODE area,
; so they are all READONLY (i.e. cannot be modified at run-time),
; but they can be initialized at assembly-time to any value

Prompt1		DCB	"Please enter in the length of the array (0 <= length <= 20): ", 0	; A prompt to get input from user
Prompt2		DCB "Please enter in the integers of the array (in order): \n", 0	; A prompt to get input from user
ProbeNum	DCB	"\nNumber ", 0
PosResult	DCB	" is in your Array. \n", 0 ; This informs USER a value is in the Array
NegResult	DCB	" is NOT in your Array. \n", 0 ; This informs USER a value is NOT in the Array
Prompt3		DCB "Enter in a value to search for in the array: ", 0 ; Prompt user for value to search for
Prompt4		DCB "Would you like to search for a value in the array? (Y/y = yes, else = NO) \n", 0 ; Ask user if they want to serach for a value in the array
Prompt5		DCB	"Would you like to search for another value? (Y/y = yes, else = NO) \n", 0 ; Ask if user wants to search for another value
GoodBye		DCB	"Thanks for sharing your array (: GOODBYE!\n", 0

	ALIGN
		
; The following data items are in the DATA area,
; so they are all READWRITE (i.e. can be modified at run-time),
; but are automatically initialized at assembly-time to zeroes 

	AREA    MyData, DATA, READWRITE
		
	ALIGN

Array		SPACE	80	; Make space for 20 integers (20 * 4 = 80 bytes)
ArraySize	SPACE	4	; The space for holding the size of the array
Value		SPACE	4	; The space for holding user search value


;-------------------- END OF MODIFIABLE CODE ----------------------

	ALIGN

	END			; end of source program in this file