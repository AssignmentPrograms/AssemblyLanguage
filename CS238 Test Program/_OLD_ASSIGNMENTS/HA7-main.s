; File: HA7-main.s
; Author: Brandon Jones

; This file needs to be in a Keil version 5 project, together with file init.s

; This is an initial demo program for HA4, which you need to change to complete HA4

; All future CS 238 Home Assignments, will have similar but different files,
; like HA5-main.s, HA6-main.s, etc.

; Executable code in HAx-main.s files should start at label main

	EXPORT	main		; this line is needed to interface with init.s

; Usable utility functions defined in file init.s
; Importing any label from another source file is necessary
; in order to use that label in this source file

	IMPORT	GetCh		; Input one ASCII character from the UART #1 window (from keyboard)
	IMPORT	PutCh		; Output one ASCII character to the UART #1 window
	IMPORT	PutCRLF		; Output CR and LF to the UART #1 window
    IMPORT	UDivMod		; Perform unsigned division to obtain quotient and remainder
	IMPORT	GetDec		; Input a signed number from the UART #1 window
	IMPORT	PutDec		; Output a signed number to the UART #1 window
	IMPORT	GetStr		; Input a CR-terminated ASCII string from the UART #1 window
	IMPORT	PutStr		; Output null-terminated ASCII string to the UART #1 window
	;IMPORT	FindGCD		; Compute the Greatest Common Divisor for two integers
						; Input: R0, R1		Output: R0

	AREA    MyCode, CODE, READONLY

	ALIGN			; highly recommended to start and end any area with ALIGN

; Start of executable code is at following label: main

main

;-------------------- START OF MODIFIABLE CODE ----------------------

	PUSH	{LR}		; save return address of caller in init.s
	
Again
	
	; Aquire values for GCD calculation
	LDR R0, =IntroPrompt
	BL PutStr
	
	; Obtain the first integer
Int1NotPos
	LDR R0, =InputPrompt1
	BL PutStr
	BL GetDec
	CMP R0, #0
	BLO Int1NotPos
	LDR R1, =Integer1
	STR R0, [R1] ; I understand could be done in an easier way (w/ MOV), but for practice
				 ; R1 = First Itneger ('a')
	
	; Obtain the second integer
Int2NotPos
	LDR R0, =InputPrompt2
	BL PutStr
	BL GetDec
	CMP R0, #0
	BLO Int2NotPos
	LDR R2, =Integer2
	STR R0, [R2] ; I understand could be done in an easier way (w/ MOV), but for practice
				 ; R2 = Second Integer ('b')
				 
	; Restore the integers into their appropriate registers to be used in 'FindGCD'
	LDR R0, [R1]
	LDR R1, [R2]
	
	; Find the GCD
	BL FindGCD
	
	; Store the GCD answer in R3
	LDR R3, =GCD
	STR R0, [R3]
	
	; Output the final answer prompt
	LDR R0, =OutputPrompt1
	BL PutStr
	
	; Load the R0 with answer to be printed
	LDR R0, [R3]
	BL PutDec
	
	; Ask if user wishes to compute GCD for new numbers
	LDR R0, = RepeatPrompt
	BL PutStr
	BL GetCh
	BL PutCRLF
	BL PutCRLF
	CMP R0, #'y'
	BEQ Again
	CMP R0, #'Y'
	BEQ Again
	
	; That is all! (:
	LDR R0, =GoodbyePrompt
	BL PutStr







	POP {PC}
	
	
	
;--------------------------------FUNCTION AREA-----------------

;----------------------GCD-----------------------
; Compute the GCD for two input values
; Registers: R0 is register containing Value1 (a) . R1 is register containing Value2 (b).
; Output: In R0, contains the GCD of the two numbers
FindGCD
	PUSH	{LR, R1} ; Preserve all "scratchpad" registers except for the return register (R0)
				 ; Also preserve the current PC to be later
							
	; GCD algoritm
	; if ( a > b )
	CMP R0, R1
	BLS NoFirstThen
		; then
		SUB R0, R1 ; a = a - b
		BL FindGCD
		POP {PC, R1}
NoFirstThen
	; if ( a < b)
	CMP R0, R1
	BHS NoSecondThen
		;then
		SUB R1, R0
		BL FindGCD ; Recursively call this function
		POP {PC, R1}
NoSecondThen
	
	POP		{PC, R1} ; Restore the registers as they were and restore the PC value
;--------------------------------FUNCTION AREA-----------------


; Some commonly used ASCII codes

CR	EQU	0x0D	; Carriage Return (to move cursor to beginning of current line)
LF	EQU	0x0A	; Line Feed (to move cursor to same column of next line)

; The following data items are in the CODE area,
; so they are all READONLY (i.e. cannot be modified at run-time),
; but they can be initialized at assembly-time to any value
	
IntroPrompt		DCB 	"I will find the GCD for two given integers.", 0
InputPrompt1	DCB 	"\nPlease enter the first POSITIVE integer: ", 0
InputPrompt2	DCB		"\nPlease enter the second POSITIVE integer: ", 0
OutputPrompt1	DCB		"\nThe Greatest Common Divisor is: ", 0
RepeatPrompt	DCB		"\nWould you like to give it another go? (y/n): ", 0
GoodbyePrompt	DCB		"\nGoodbye!", 0

	ALIGN
		
; The following data items are in the DATA area,
; so they are all READWRITE (i.e. can be modified at run-time),
; but are automatically initialized at assembly-time to zeroes 

	AREA    MyData, DATA, READWRITE
		
	ALIGN

Integer1	SPACE	4 ; Space for first integer
Integer2	SPACE	4 ; Space for second integer
GCD			SPACE	4 ; Space for the GCD Answer

;-------------------- END OF MODIFIABLE CODE ----------------------

	ALIGN

	END			; end of source program in this file