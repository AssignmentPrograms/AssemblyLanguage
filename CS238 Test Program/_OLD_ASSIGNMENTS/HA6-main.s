; File: HA6-main.s
; Author: Brandon Jones

; This file needs to be in a Keil version 5 project, together with file init.s

; This is an initial demo program for HA4, which you need to change to complete HA4

; All future CS 238 Home Assignments, will have similar but different files,
; like HA5-main.s, HA6-main.s, etc.

; Executable code in HAx-main.s files should start at label main

	EXPORT	main		; this line is needed to interface with init.s

; Usable utility functions defined in file init.s
; Importing any label from another source file is necessary
; in order to use that label in this source file

	IMPORT	GetCh		; Input one ASCII character from the UART #1 window (from keyboard)
	IMPORT	PutCh		; Output one ASCII character to the UART #1 window
	IMPORT	PutCRLF		; Output CR and LF to the UART #1 window
    IMPORT	UDivMod		; Perform unsigned division to obtain quotient and remainder
	IMPORT	GetDec		; Input a signed number from the UART #1 window
	IMPORT	PutDec		; Output a signed number to the UART #1 window
	IMPORT	GetStr		; Input a CR-terminated ASCII string from the UART #1 window
	IMPORT	PutStr		; Output null-terminated ASCII string to the UART #1 window
	IMPORT	FindGCD		; Computes and returns (in R0) the GCD value of two integers (in R0 and R1)

	AREA    MyCode, CODE, READONLY

	ALIGN			; highly recommended to start and end any area with ALIGN

; Start of executable code is at following label: main

main

;-------------------- START OF MODIFIABLE CODE ----------------------

	PUSH	{LR}		; save return address of caller in init.s
	
	; ----------------- GET SIZE OF ARRAY: -----------------
Repeat
	;Repeat
	LDR R0, =Prompt1 ; Load R0 w/ the address of Prompt1 (Prompt1 points to the first byte in null term. string)
	BL PutStr ; Call the user defined "PutStr" 'function' to print the supposed string in R0
	BL GetDec ; Get user input (This should be the value for the size of the array)
	;Until
	CMP R0, #0
	BLT Repeat ; Repeat if entered value is negative
	BEQ ArraySizeZero
	CMP R0, #20
	BGT Repeat ; Repeat if entered value is greater than 20
	
	LDR R1, =ArraySize ; R1 = Address of ArraySize
	STR R0, [R1] ; We will override R0 soon, store what is in R0 in the memory address R1
	
	
	; ----------------- FILL THE ARRAY: -----------------
	
	MOV R3, #0
	
	LDR R0, =Prompt2 ; R0 = Address of first byte named Prompt2
	BL PutStr
	
	LDR R2, =Array ; Load R2 with the address of Array, representing the first byte in the "Array"
	;LDR R4, =Array ; IGNOREIGNOREIGNOREIGNOREIGNOREIGNOREIGNOREIGNOREIGNOREIGNOREIGNOREIGNORE
	
	LDR R1, [R1] ; Load the value that was stored the address of R1 and put it in R1
LoopAgain
	;For
	BL GetDec ; Get the values for the array
	STR R0, [R2] ; Store what the user entered at the memory address R2
	ADD R2, #4 ; The user entered a 4 byte value, and we want to simulate an array, so add 4 bytes to R2, serving as our address
	ADD R3, #1 ; Add one to our counter
	CMP R3, R1 ; Check to see if we have reached the size of our array yet
	BLT LoopAgain ; If not, loop again to allow the user enter in more values
	; end for
	; Values retrieved -> continue with the program
	
	
	LDR R1, =ArraySize ; Load R1 w/ the address named "ArraySize" (First byte representing array size)
	LDR R1, [R1] ; Retreive the array size from the memory address at ArraySize
	
	; ----------------- The Search Begins: -----------------
	
	LDR R0, =Prompt4 ; Load R0 with address of first bye (known as Prompt4), of the null terminated string
	BL PutStr ; Prompt user for option to search for an element in the array
	BL GetCh ; Take user input (Char value)

While
	;While
	MOV R3, #0 ; R3 = Starting Index 'i'
	BL PutCRLF ; NEW LINE (Carriage Return and Line Feed)
	CMP R0, #'Y' ; If the user entered Y Search...
	BEQ SearchAgain
	CMP R0, #'y' ; If the user entered y Search...
	BNE EndWhile ; If the user did not enter either 'y' or 'Y', then break out of the loop
SearchAgain
	LDR R4, =Array ; Load the address of teh first array
	LDR R0, =Prompt3
	BL PutStr
	BL GetDec ; R0 = Value to Search For	->	'V'
	LDR R7, =Value ; Load R7 with address of Value (the first byte of a 4 byte number)
	STR R0, [R7] ; Store the value the user wants to search for into address of R7 (address at Value)
	MOV R5, #0 ; Found = False	->	R5 = Found TruthValue (0/1)
LOOPY
	;For
	LDR R6, [R4] ; Value to Compare 'V' (R0) to.
	CMP R0, R6 ; Compare the search value 'V' to the current array value R6
	BNE NotFound
	MOV R5, #1 ; Set our Found TruthValue to 1	->	Found = TRUE
	BAL BREAK
NotFound
	ADD R4, #4 ; Add four bytes (that of an integer) to move address to next decimal value
	ADD R3, #1 ; Increment our counter by 1
	CMP R3, R1 ; Check to see if we have reached the end of the 'Array'
	BLT LOOPY ; If we have not reached the end of our 'Array', then loop again
	;end for
BREAK
	;if
	CMP R5, #1 ; Check and see if value was found
	BNE NegativeCaptain ; If value was NOT found then inform user as such
	;then
	LDR R0, =ProbeNum ; Load R0 with the address of the first byte ProbeNum
	BL PutStr ; Put that string starting at the Address of first byte ProbeNum to the null termination 0
	LDR R0, [R7] ; Load the value that was stored in R7 into register R0
	BL PutDec ; Print that Value
	LDR R0, =PosResult ; Load R0 with address of the first byte (called PosResult) in string
	BL PutStr ; Print that string
	
	MOV R0, R3 ; Store the index that was a match (R3 was the index, move to R0 for processing)
	BL PutDec ; Call PutDec to print out value inside the R0 register
	BL PutCRLF ; New Line to be clean.
	BL PutCRLF ; New Line to separate result from future prompts
	
	B PromptTime
NegativeCaptain
	;else
	LDR R0, =ProbeNum ; Load R0 with adderss of ProbeNum
	BL PutStr ; Print that string starting from the first byte ProbeNum to the null termination
	LDR R0,[R7] ; Load R0 with the value at the address of R7 (This is the value of the search)
	BL PutDec ; Print that value
	LDR R0, =NegResult ; Load R0 with address of NegResult (the first byte in a string)
	BL PutStr ; Print that string
	;end if
PromptTime
	LDR R0, =Prompt5 ; Load R0 with first byte Prompt5 in a null terminated string
	BL PutStr ; Print that string
	BL GetCh ; Retrieve user input
	BL PutCRLF ; NEW LINE (Carriage Return and Line Feed)
	BAL While ; Do it again? (:
EndWhile
	;end while
	
	
	
	; BELOW WILL PRINT OUT EACH ELEMENT IN THE ARRAY (For Debugging)
	
	;LDR R1, =ArraySize ; R1 = Address of ArraySize
	;LDR R1, [R1]
	;;MOV R3, #0
;LOOPY2
	;For
	;LDR R0, =ProbeNum
	;BL PutStr
	;LDR R0, [R4]
	;BL PutDec
	;LDR R0, =Value
	;BL PutStr
	;ADD R4, #4
	;ADD R3, #1
	;CMP R3, R1
	;BLT LOOPY2
	
	LDR R0, =GoodBye ; Load R0 with address of first byte (GoodBye) in a nullterminated string
	BL PutStr

	POP {PC}
	
ArraySizeZero ; Special case of zero array
	LDR R0, =ZeroArray ; Load R0 with the address of the first byte in a null terminated string (ZeroArray)
	BL PutStr ; Print that string
	B EndWhile ; Using a Label already created, and to allow for goodbye message.

; Some commonly used ASCII codes

CR	EQU	0x0D	; Carriage Return (to move cursor to beginning of current line)
LF	EQU	0x0A	; Line Feed (to move cursor to same column of next line)

; The following data items are in the CODE area,
; so they are all READONLY (i.e. cannot be modified at run-time),
; but they can be initialized at assembly-time to any value

Prompt1		DCB	"Please enter in the length of the array (0 <= length <= 20): ", 0	; A prompt to get input from user
Prompt2		DCB "Please enter in the integers of the array (in order): \n", 0	; A prompt to get input from user
ProbeNum	DCB	"\nNumber ", 0
PosResult	DCB	" is in your Array. It was found at position (0 indexed):\n", 0 ; This informs USER a value is in the Array
NegResult	DCB	" is NOT in your Array. \n", 0 ; This informs USER a value is NOT in the Array
Prompt3		DCB "Enter in a value to search for in the array: ", 0 ; Prompt user for value to search for
Prompt4		DCB "Would you like to search for a value in the array? (Y/y = yes, else = NO) \n", 0 ; Ask user if they want to serach for a value in the array
Prompt5		DCB	"Would you like to search for another value? (Y/y = yes, else = NO) \n", 0 ; Ask if user wants to search for another value
GoodBye		DCB	"Thanks for sharing your array (: GOODBYE!\n", 0 ; Inform user program is over
ZeroArray	DCB "\n\nYou have entered an array of size 0, no elements\n\n", 0 ; Inform user there are no elements in array

	ALIGN
		
; The following data items are in the DATA area,
; so they are all READWRITE (i.e. can be modified at run-time),
; but are automatically initialized at assembly-time to zeroes 

	AREA    MyData, DATA, READWRITE
		
	ALIGN

Array		SPACE	80	; Make space for 20 integers (20 * 4 = 80 bytes)
ArraySize	SPACE	4	; The space for holding the size of the array
Value		SPACE	4	; The space for holding user search value


;-------------------- END OF MODIFIABLE CODE ----------------------

	ALIGN

	END			; end of source program in this file