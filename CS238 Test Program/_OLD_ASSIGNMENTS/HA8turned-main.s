; File: HA8-main.s
; Author: Brandon Jones

; This file needs to be in a Keil version 5 project, together with file init.s

; This is an initial demo program for HA4, which you need to change to complete HA4

; All future CS 238 Home Assignments, will have similar but different files,
; like HA5-main.s, HA6-main.s, etc.

; Executable code in HAx-main.s files should start at label main

	EXPORT	main		; this line is needed to interface with init.s

; Usable utility functions defined in file init.s
; Importing any label from another source file is necessary
; in order to use that label in this source file

	IMPORT	GetCh		; Input one ASCII character from the UART #1 window (from keyboard)
	IMPORT	PutCh		; Output one ASCII character to the UART #1 window
	IMPORT	PutCRLF		; Output CR and LF to the UART #1 window
    IMPORT	UDivMod		; Perform unsigned division to obtain quotient and remainder
	IMPORT	GetDec		; Input a signed number from the UART #1 window
	IMPORT	PutDec		; Output a signed number to the UART #1 window
	IMPORT	GetStr		; Input a CR-terminated ASCII string from the UART #1 window
	IMPORT	PutStr		; Output null-terminated ASCII string to the UART #1 window
	IMPORT	FindGCD		; Compute the Greatest Common Divisor for two integers
	;IMPORT	PutBin		; Take a Decimal value and print the Binary equilivant

	AREA    MyCode, CODE, READONLY

	ALIGN			; highly recommended to start and end any area with ALIGN

; Start of executable code is at following label: main

main

;-------------------- START OF MODIFIABLE CODE ----------------------

	PUSH	{LR}		; save return address of caller in init.s
	
Again
	LDR R0, =Intro
	BL PutStr
	
	LDR R0, =Prompt1 ; Load R0 with the first byte of our prompt to be printed
	BL PutStr ; Call PutStr to print our string, from the first byte to null char
	BL GetDec ; Retrieve User Input for a value to be later toyed with.
	LDR R3, =Value
	STR R0, [R3]
	
	BL PutDec
	MOV R1, R0 ; Save user entered value for future use
	LDR R0, =ResultP1
	BL PutStr
	
	MOV R0, R1 ; Restore user entered value to be converted to binary
	BL PutBin
	
	LDR R0, =ResultP2
	BL PutStr ; Prompt the user with a response
	
	LDR R0, [R3]
	
	CMP R0, #0
	BEQ ByeBye
	
	;LDR R0, =Option
	;BL PutStr
	;BL GetCh
	
	BL PutCRLF
	;BL PutCRLF
	;CMP R0, #'y'
	;BEQ Again
	;CMP R0, #'Y'
	;BEQ Again
	B Again
	
ByeBye
	LDR R0, =GoodBye
	BL PutStr
	
	POP {PC}
	
	
;---------------------PutBin----------------------
; Prints the Binary Equilivant of an inputed Decimal Value
; Registers: R0 is register containing Decimal Value
PutBin
	PUSH {LR, R1-R3}	;	Store any used registers to save possible user applied values
	
	MOV R2, #0 ; This will be a counter to track new additions to the stack
Loop
	MOV R1, #2 ; We will be dividing our decimal value by 2 to obtain the Binary Equilivant
	BL UDivMod ; Will divide R0 by R1 and return R0 = Remainder, and R1 = Quotient
	ADD R3, R0, #'0' ; Translate to ASCII and store in R3
	PUSH {R3}
	ADD R2, #1 ; R2 is our counter, up it by one.
	MOV R0, R1 ; For a potential future DIV to have the correct value in R0
	CMP R1, #0 ; If the Quotient is 0 then there is no need to divide further
	BGT Loop
	
	
AnotherChar
	POP{R0}
	BL PutCh
	SUB R2, #1
	CMP R2, #0
	BGT AnotherChar
	
	POP {PC, R1-R3}	; Restore any used Registers back to original value.


; Some commonly used ASCII codes

CR	EQU	0x0D	; Carriage Return (to move cursor to beginning of current line)
LF	EQU	0x0A	; Line Feed (to move cursor to same column of next line)

; The following data items are in the CODE area,
; so they are all READONLY (i.e. cannot be modified at run-time),
; but they can be initialized at assembly-time to any value

Intro		DCB		"Enter in 0 to quit the program.\n", 0
Prompt1		DCB		"Please enter in a Decimal value to be converted to Binary: ", 0
ResultP1	DCB		" in Decimal is ", 0
ResultP2	DCB		" in Binary.\n", 0
Option		DCB		"\nWould you like to enter in a new value? (y/n): ", 0
GoodBye		DCB		"\nGoodbye!", 0

	ALIGN
		
; The following data items are in the DATA area,
; so they are all READWRITE (i.e. can be modified at run-time),
; but are automatically initialized at assembly-time to zeroes 

	AREA    MyData, DATA, READWRITE
		
	ALIGN

;Array		SPACE	80	; Make space for 20 integers (20 * 4 = 80 bytes)
;ArraySize	SPACE	4	; The space for holding the size of the array
Value		SPACE	4	; The space for holding user search value


;-------------------- END OF MODIFIABLE CODE ----------------------

	ALIGN

	END			; end of source program in this file