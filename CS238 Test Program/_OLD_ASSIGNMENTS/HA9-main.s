; File: HA9-main.s
; Author: Brandon Jones

; This file needs to be in a Keil version 5 project, together with file init.s

; This is an initial demo program for HA4, which you need to change to complete HA4

; All future CS 238 Home Assignments, will have similar but different files,
; like HA5-main.s, HA6-main.s, etc.

; Executable code in HAx-main.s files should start at label main

	EXPORT	main		; this line is needed to interface with init.s
	EXPORT gcd

	PRESERVE8

; Usable utility functions defined in file init.s
; Importing any label from another source file is necessary
; in order to use that label in this source file

	IMPORT	GetCh		; Input one ASCII character from the UART #1 window (from keyboard)
	IMPORT	PutCh		; Output one ASCII character to the UART #1 window
	IMPORT	PutCRLF		; Output CR and LF to the UART #1 window
    IMPORT	UDivMod		; Perform unsigned division to obtain quotient and remainder
	IMPORT	GetDec		; Input a signed number from the UART #1 window
	IMPORT	PutDec		; Output a signed number to the UART #1 window
	IMPORT	GetStr		; Input a CR-terminated ASCII string from the UART #1 window
	IMPORT	PutStr		; Output null-terminated ASCII string to the UART #1 window
	;IMPORT	FindGCD		; Compute the Greatest Common Divisor for two integers
	;IMPORT	PutBin		; Take a Decimal value and print the Binary equilivant
	IMPORT gcd6

	AREA    MyCode, CODE, READONLY

	ALIGN			; highly recommended to start and end any area with ALIGN

; Start of executable code is at following label: main

main

;-------------------- START OF MODIFIABLE CODE ----------------------

	PUSH	{LR}		; save return address of caller in init.s

ProgramLoop

	LDR R0, =Prompt1
	BL PutStr

	MOV R10, #1 ; This will be used as our counter register
	
	LDR R9, =Array ; Load R9 with the address of the first byte in our array
	
	; Begin Loop for values
GotNeg
	LDR R0, =GetVal
	BL PutStr
	
	MOV R0, R10
	BL PutDec
	
	LDR R0, =ValPrompt
	BL PutStr
	
	BL GetDec
	
	CMP R0, #0
	BLT GotNeg ; Compare the value as if it is signed (since we want to check a potentailly signed value)
	; Got a valid value past this point
	STR R0, [R9]
	ADD R9, #4 ; Add four bytes to our array to access the next element
	ADD R10, #1
	CMP R10, #6
	BLS GotNeg ; Reusing the previous label, if we still have more numbers to get, go get 'em
	; End loop, we have got the 6 values we want to use in the GCD calculation
	
	
	;LDR R0, =Array ; Restore our array back to the first byte
	SUB R9, #4 ; Subtract 4 bytes from our array memory address to obtain the last value in the array
	MOV R10, #1 ; We will reuse our counter register
	
	; 1. PLACED INPUT PARAMETERS
	; Loop for loading values into appropraite registers
Loop1
	LDR R1, [R9] ; Load the appropraite number
	PUSH {R1} ; Push this value onto the stack
	SUB R9, #4 ; Subtract four bytes from our array memory addresss to obtain the next number
	ADD R10, #1 ; Add to our counter
	CMP R10, #2 ; We only want to push the last two values onto the stack
	BLS Loop1 ; If we have not pushed the two values onto the stack then keep looping. (Will happen exactly twice)
	; End of loop
	
	; Our counter register (R2) should now have #3, the remaining values in the array will be put into registers

	; Store the remaining values
	LDR R3, [R9]
	SUB R9, #4
	LDR R2, [R9]
	SUB R9, #4
	LDR R1, [R9]
	SUB R9, #4
	LDR R0, [R9]
	
	; We should have the registeres loaded with the appropraite values...
	
	; 2. CALLED FUNCTION
	BL gcd6 ; Call GCD to calculate the gcd
	
	; 3. REMOVE STACK PARAMETERS (POP AND DISCARD)
	ADD SP, #2*4 ; We wish to pop and discard two digits which take up 4 bytes each, so just move the SP down 8 times (4*2)
	
	; 4. EXPECT FUNCTION OUTPUT TO BE IN R0
	MOV R1, R0 ; Store the result for safe keeping.
	
	
	;LDR R9, #4
	LDR R0, =Array
	MOV R10, #1
	
	LDR R0, =Result
	BL PutStr
	
	; The below loop is for output readibility
	; Begin Loop
Loop2
	LDR R2, [R9]
	ADD R9, #4
	ADD R10, #1
	
	CMP R10, #7
	BLO NoAnd
	LDR R0, =AndWord
	BL PutStr
NoAnd
	MOV R0, R2
	BL PutDec
	
	CMP R10, #6
	BHS NoComma ; If we are on the last value do not print a command and a space.
	LDR R0, =LiteralComma
	BL PutStr
NoComma

	CMP R10, #7
	BHS NoSpace
	LDR R0, =LiteralSpace
	BL PutStr
NoSpace
	
	CMP R10, #6
	BLS Loop2
	; End Loop
	
	
	; Time to prompt user with result
	LDR R0, =ResultPrompt
	BL PutStr
	MOV R0, R1
	BL PutDec
	BL PutCRLF
	
	; Ask if user would like to repeat the program
	LDR R0, =RepeatPrompt
	BL PutStr
	BL GetCh
	
	BL PutCRLF
	BL PutCRLF
	
	; Determine user's response
	CMP R0, #'y'
	BEQ ProgramLoop
	CMP R0, #'Y'
	BEQ ProgramLoop

	; A goodbye message to end the program
	LDR R0, =GoodBye
	BL PutStr

	POP {PC}



;----------------------GCD-----------------------
; Compute the GCD for two input values
; Registers: R0 is register containing Value1 (a) . R1 is register containing Value2 (b).
; Output: In R0, contains the GCD of the two numbers
gcd
	PUSH	{LR, R1} ; Preserve all "scratchpad" registers except for the return register (R0)
				 ; Also preserve the current PC to be later
							
	; GCD algoritm
	; if ( a > b )
	CMP R0, R1
	BLS NoFirstThen
		; then
		SUB R0, R1 ; a = a - b
		BL gcd
		POP {PC, R1}
NoFirstThen
	; if ( a < b)
	CMP R0, R1
	BHS NoSecondThen
		;then
		SUB R1, R0
		BL gcd ; Recursively call this function
		POP {PC, R1}
NoSecondThen
	
	POP		{PC, R1} ; Restore the registers as they were and restore the PC value
	



; Some commonly used ASCII codes

CR	EQU	0x0D	; Carriage Return (to move cursor to beginning of current line)
LF	EQU	0x0A	; Line Feed (to move cursor to same column of next line)

; The following data items are in the CODE area,
; so they are all READONLY (i.e. cannot be modified at run-time),
; but they can be initialized at assembly-time to any value

Prompt1			DCB		"I will calculate the GCD for 6 provided NON-NEGATIVE numbers.\n", 0 ; A welcoming prompt
GetVal			DCB		"Please enter in the ", 0
ValPrompt		DCB		" (st/nd/rd/th) NON-NEGATIVE value to be used in calculating a GCD: ", 0 ; Prompt for values
Result			DCB		"The GCD of ", 0
LiteralComma	DCB		",", 0 ; To provide some separation between values
LiteralSpace	DCB		" ", 0
AndWord			DCB		"and ", 0 ; for the sake of the english language
ResultPrompt	DCB		" is: ", 0 ; Result of calculation Prompt
RepeatPrompt	DCB		"\nNICE! Would you like to go again? (y/n): ", 0 ; Option to repeat
GoodBye			DCB		"\nGOODBYE!", 0 ; Goodbye prompt



;first GCD Value", 0 ; A prompt for first value
GetVal2		DCB		"Please enter in the second GCD Value", 0 ; A prompt for second value
GetVal3		DCB		"Please enter in the third GCD Value", 0 ; A prompt for third value
GetVal4		DCB		"Please enter in the fourth GCD Value", 0 ; A prompt for fourth value
GetVal5		DCB		"Please enter in the fifth GCD Value", 0 ; A prompt for fifth value
GetVal6		DCB		"Please enter in the sixth GCD Value", 0 ; A prompt for sixth value

	ALIGN
		
; The following data items are in the DATA area,
; so they are all READWRITE (i.e. can be modified at run-time),
; but are automatically initialized at assembly-time to zeroes 

	AREA    MyData, DATA, READWRITE
		
	ALIGN

Array		SPACE	24	; Make space for 6 integers (6 * 4 = 24 bytes)

;Array		SPACE	80	; Make space for 20 integers (20 * 4 = 80 bytes)
ArraySize	SPACE	4	; The space for holding the size of the array
Value		SPACE	4	; The space for holding user search value


;-------------------- END OF MODIFIABLE CODE ----------------------

	ALIGN

	END			; end of source program in this file