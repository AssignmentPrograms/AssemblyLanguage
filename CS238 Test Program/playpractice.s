; File: HA4-main.s
; Author: Brandon Jones

; This file needs to be in a Keil version 5 project, together with file init.s

; This is an initial demo program for HA4, which you need to change to complete HA4

; All future CS 238 Home Assignments, will have similar but different files,
; like HA5-main.s, HA6-main.s, etc.

; Executable code in HAx-main.s files should start at label main

	EXPORT	main		; this line is needed to interface with init.s
; Usable utility functions defined in file init.s
; Importing any label from another source file is necessary
; in order to use that label in this source file

	IMPORT	GetCh		; Input one ASCII character from the UART #1 window (from keyboard)
	IMPORT	PutCh		; Output one ASCII character to the UART #1 window
	IMPORT	PutCRLF		; Output CR and LF to the UART #1 window
    IMPORT	UDivMod		; Perform unsigned division to obtain quotient and remainder
	IMPORT	GetDec		; Input a signed number from the UART #1 window
	IMPORT	PutDec		; Output a signed number to the UART #1 window
	IMPORT	GetStr		; Input a CR-terminated ASCII string from the UART #1 window
	IMPORT	PutStr		; Output null-terminated ASCII string to the UART #1 window

	AREA    MyCode, CODE, READONLY

	ALIGN			; highly recommended to start and end any area with ALIGN

; Start of executable code is at following label: main

main

;-------------------- START OF MODIFIABLE CODE ----------------------

	PUSH	{LR}		; save return address of caller in init.s
	
	; Task: Take in a positive integer n and print the integers
	;					from 1 to n for which the Baum-Sweet sequence returns true
	;The Baum-Sweet sequence:
	;Returns falsy if binary representation of # has odd number of consecutive zeros anywhere in the number
	;Returns truthy otherwise.


Again
	LDR R0, =Prompt
	BL PutStr
	
	BL GetDec ; R0 = Number to stop at
	BL BaumSweet ; Call the function to check the value
	
	MOV R1, R0 ; Preserve value
	
	LDR R0, =Result
	BL PutStr
	
	MOV R0, R1
	BL PutDec
	
	LDR R0, =Repeat
	BL PutStr
	
	BL GetCh
	BL PutCRLF
	BL PutCRLF
	
	CMP R0, #'y'
	BEQ Again
	CMP R0, #'Y'
	BEQ Again
	
	
	
	LDR R0, =GoodBye
	BL PutStr

	POP {R1, R2, PC}
	
	; R0 = Number to stop at
BaumSweet
	PUSH{LR,R1,R2,R3,R4}
	MOV R1, #32 ; We have 32 bits to go through
	MOV R2, #0 ; Counter for consecutive zeros
	
	CMP R0, #0
	BLT NoLeadingZeros
	
LeadingZeros
	MOVS R0, R0, LSL #1 ; Get rid of leading zeros
	SUB R1, #1 ; One less bit to worry about
	BPL LeadingZeros
	
	MOV R0, R0, LSR #1
	ADD R1, #1
	
	; We should now have a clean representation for our value (No leading zeros)
NoLeadingZeros
	MOV R0, R0, LSL #1
	SUB R1, #1
	
	CMP R1, #0
	BLE Test

	CMP R0, #0
	BLT Test ; If there is a 1 in the MSB place then we are ready to test our counted zeros
	ADD R2, #1
	B NoLeadingZeros ; Keep counting the zeros
	
Test
	MOV R3, R0 ; Preserve the register
	MOV R4, R1 ; Preserve the register
	MOV R0, R2
	MOV R1, #2
	BL UDivMod ; R0 = Remainder		R1 = Quotient
	CMP R0, #1 ; If R0 = #0 then there is no remainder and the number is even
	BEQ Found ; R0 = 1 and we will treat 1 as a (Yes there are consecutive odd # zeros)
	MOV R1, R4
	MOV R0, R3
	
	CMP R1, #0
	BLE Done
	
Skip
	MOV R2, #0 ; Previous bunch of ones passed the test
	B NoLeadingZeros
	
Done
	MOV R0, #0 ; 0 Means that there were no consecutive odd zeros
	
Found
	POP{PC,R1,R2,R3,R4}
	
	

Prompt	DCB	"Hello! Please enter in a number to be ran through the Baum-Sweet sequence: ", 0
Result	DCB	"\nYour result is (0 for NO consecutive odd# zeros and 1 for YES consecutive odd# zeros): ", 0
Repeat	DCB	"\n\nAgain? (y/n): ", 0
GoodBye	DCB	"\nGOODBYE! (:", 0

	ALIGN
		
; The following data items are in the DATA area,
; so they are all READWRITE (i.e. can be modified at run-time),
; but are automatically initialized at assembly-time to zeroes 

	AREA    MyData, DATA, READWRITE
		
	ALIGN
		
;-------------------- END OF MODIFIABLE CODE ----------------------

	ALIGN

	END			; end of source program in this file