; File: HA4-main.s
; Author: Brandon Jones

; This file needs to be in a Keil version 5 project, together with file init.s

; This is an initial demo program for HA4, which you need to change to complete HA4

; All future CS 238 Home Assignments, will have similar but different files,
; like HA5-main.s, HA6-main.s, etc.

; Executable code in HAx-main.s files should start at label main

	EXPORT	main		; this line is needed to interface with init.s

; Usable utility functions defined in file init.s
; Importing any label from another source file is necessary
; in order to use that label in this source file

	IMPORT	GetCh		; Input one ASCII character from the UART #1 window (from keyboard)
	IMPORT	PutCh		; Output one ASCII character to the UART #1 window
	IMPORT	PutCRLF		; Output CR and LF to the UART #1 window
    IMPORT	UDivMod		; Perform unsigned division to obtain quotient and remainder
	IMPORT	GetDec		; Input a signed number from the UART #1 window
	IMPORT	PutDec		; Output a signed number to the UART #1 window
	IMPORT	GetStr		; Input a CR-terminated ASCII string from the UART #1 window
	IMPORT	PutStr		; Output null-terminated ASCII string to the UART #1 window

	AREA    MyCode, CODE, READONLY

	ALIGN			; highly recommended to start and end any area with ALIGN

; Start of executable code is at following label: main

main

;-------------------- START OF MODIFIABLE CODE ----------------------

	PUSH {LR}		; save return address of caller in init.s
	
	; CODEGOLF challenge, Testing the validity of a tipping die roll program
	
	
	
AGAIN
	LDR R0, =WelcomePrompt
	BL PutStr

	; Step 1: Input the values

	LDR R1, =Array ; Load the array address (address of first of the bytes)
	MOV R2, #9 ; Our counter for number of accepted numbers					R2 = Counter
	
MoreValues
	; Do
	BL GetDec ; get the value of the first integer
	STR R0, [R1] ; Store the value in the array
	ADD R1, #4 ; Increment the array pointer to point to next integer
	SUBS R2, #1 ; Decrement our counter
	BNE MoreValues
	; While Counter (R2) != 0
	
	; Step 2: Validate Each array value
	
	LDR R1, =Array ; Set the array pointer back to the first byte
	MOV R3, #1 ; R3 = Truth value, 0 for false 1 for true
	MOV R2, #9 ;															R2 = Counter
	
NextCheck
	; Do
	LDR R4, [R1] ; R4 = Previous element
	
	ADD R1, #4 ; Increment our array pointer to the next element
	LDR R0, [R1] ; R0 = Current element (NOTE: We start by assuming the first digit is valid)
	; VALIDITY CHECK 1: NEXT DIGIT CAN NOT BE SAME AS PREVIOUS DIGIT
	CMP R0, R4 ; Is R0 != R4
	BNE Continue1
		MOV R3, #0 ; Set truth value to false
		B FoundFalse
Continue1
	; VALIDITY CHECK 2: NEXT DIGIT CAN NOT BE EQUAL TO 7 MINUS THE PREVIOUS DIGIT
	RSB R5, R4, #7 ; 7 Minus the previous element
	CMP R0, R5 ; Compare our current element to 7 minus the previous element
	BNE Continue2
		MOV R3, #0 ; Set truth value to false
		B FoundFalse
Continue2
	SUBS R2, #1 ; Decrement our counter
	BNE NextCheck
	; While Counter (R2) != 0 and Truth Value (R3) != 0
	
	LDR R0, =ValidPrompt
	BL PutStr
	B Valid

FoundFalse
	LDR R0, =InvalidPrompt
	BL PutStr
	
Valid
	
	BL PutCRLF
	MOV R0, #'A'
	BL PutCh
	MOV R0, #'g'
	BL PutCh
	MOV R0, #'a'
	BL PutCh
	MOV R0, #'i'
	BL PutCh
	MOV R0, #'n'
	BL PutCh
	MOV R0, #'?'
	BL PutCh
	MOV R0, #' '
	BL PutCh
	MOV R0, #'Y'
	BL PutCh
	MOV R0, #'/'
	BL PutCh
	MOV R0, #'N'
	BL PutCh
	MOV R0, #':'
	BL PutCh
	MOV R0, #' '
	BL PutCh
	BL GetCh
	
	BL PutCRLF
	
	CMP R0, #'Y'
	BEQ AGAIN
	CMP R0, #'y'
	BEQ AGAIN

	LDR R0, =GoodBye
	BL PutStr

	POP {PC}

; The following data items are in the CODE area,
; so they are all READONLY (i.e. cannot be modified at run-time),
; but they can be initialized at assembly-time to any value
	
WelcomePrompt	DCB		"Hello! Enter in a sequence of numbers (9) to be tested for validity: ", 0
ValidPrompt		DCB		"The assortment of values were VALID", 0
InvalidPrompt	DCB		"The assortment of values were INVALID", 0
GoodBye			DCB		"\nGOODBYE!", 0
	
	ALIGN
		
; The following data items are in the DATA area,
; so they are all READWRITE (i.e. can be modified at run-time),
; but are automatically initialized at assembly-time to zeroes 

	AREA    MyData, DATA, READWRITE
		
	ALIGN

Array	SPACE	80 ; 4 * 9 bytes for an array of 9 integers

;-------------------- END OF MODIFIABLE CODE ----------------------

	ALIGN

	END			; end of source program in this file