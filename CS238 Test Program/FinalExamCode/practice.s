; File: HA4-main.s
; Author: Brandon Jones

; This file needs to be in a Keil version 5 project, together with file init.s

; This is an initial demo program for HA4, which you need to change to complete HA4

; All future CS 238 Home Assignments, will have similar but different files,
; like HA5-main.s, HA6-main.s, etc.

; Executable code in HAx-main.s files should start at label main

	EXPORT	main		; this line is needed to interface with init.s
	EXPORT gcd
		
	PRESERVE8

; Usable utility functions defined in file init.s
; Importing any label from another source file is necessary
; in order to use that label in this source file

	IMPORT	GetCh		; Input one ASCII character from the UART #1 window (from keyboard)
	IMPORT	PutCh		; Output one ASCII character to the UART #1 window
	IMPORT	PutCRLF		; Output CR and LF to the UART #1 window
    IMPORT	UDivMod		; Perform unsigned division to obtain quotient and remainder
	IMPORT	GetDec		; Input a signed number from the UART #1 window
	IMPORT	PutDec		; Output a signed number to the UART #1 window
	IMPORT	GetStr		; Input a CR-terminated ASCII string from the UART #1 window
	IMPORT	PutStr		; Output null-terminated ASCII string to the UART #1 window
	IMPORT	gcd6

	AREA    MyCode, CODE, READONLY

	ALIGN			; highly recommended to start and end any area with ALIGN

; Start of executable code is at following label: main

main

;-------------------- START OF MODIFIABLE CODE ----------------------

	PUSH	{LR}		; save return address of caller in init.s
	
	; int x = 20;	int y = 50;		int z;		cin >> z;
	; if( z > x && z > y ) then cout << z is the largest
	; if( z < x && z < y ) then cout << z is the smallest
	; if( z > x && z < y ) then cout << z is inbetween 20 and 50
	; int k;	cin >> k;
	; if( z < k || z == k ) then cout << z is either less than k or equal to k... GUESS (:
Loop
	LDR R2, =x ; R2 = x
	LDR R3, =y ; R3 = y
	LDR R2, [R2]
	LDR R3, [R3]
	
	LDR R0, =Prompt
	BL PutStr
	
	BL GetDec
	
	LDR R1, =z ; R1 = z
	STR R0, [R1]
	
	LDR R1, [R1]
	
	; if( z > x && z > y ) then cout << z is the largest
	CMP R1, R2
	BLE NotGreater
	CMP R1, R3
	BLE NotGreater
		LDR R0, =LargestResult
		BL PutStr
		B BREAK
	
	; TODO: Make this clever. where we only use one conditional statement and branch accordingly

NotGreater
	; else if( z < x && z < y ) ; then cout << z is the smallest
	CMP R1, R2
	BGE NotLess
	CMP R1, R3
	BGE NotLess
		LDR R0, =SmallestResult
		BL PutStr
		B BREAK
	
NotLess
	; else if( z > x && z < y ) then cout << z is inbetween 20 and 50
	CMP R1, R2
	BLE NotBetween
	CMP R1, R3
	BGE NotBetween
		LDR R0, =BetweenResult
		BL PutStr
		B BREAK
NotBetween
	LDR R0, =ProbeEQResult
	BL PutStr
	
	MOV R0, R1
	BL PutDec
	
	LDR R0, =EqualResult
	BL PutStr
	
BREAK

	LDR R0, =Prompt2
	BL PutStr
	
	BL GetDec
	; Just some jibble jabber for practice
	LDR R4, =k
	STR R0, [R4]
	LDR R4, [R4]
	
	; if( z < k || z == k ) then cout << z is either less than k or equal to k... GUESS (:
	CMP R1, R4
	BLT True
	BNE False
True
		;CMP R1, R4 ; The flags must of been changed earlier (through PutStr)
		BEQ NotLessThan
		BLT LessThan
			LDR R0, =Warning
			BL PutStr
LessThan
			MOV R6, #1
			B BREAK2
NotLessThan
		MOV R6, #2

BREAK2
		LDR R0, =GuessPrompt
		BL PutStr
		
		BL GetDec
		
		CMP R0, R6
		BNE Wrong
			LDR R0, =Correct
			BL PutStr
			B GoodBye
Wrong
		LDR R0, =Incorrect
		BL PutStr
		
		B GoodBye

False
	LDR R0, =NoGuessPrompt
	BL PutStr

GoodBye
	LDR R0, =Goodbye
	BL PutStr
	
	BL PutCRLF
	BAL Loop

	POP {R1, R2, PC}

; The following data items are in the CODE area,
; so they are all READONLY (i.e. cannot be modified at run-time),
; but they can be initialized at assembly-time to any value

gcd
	PUSH {R1, LR}
	
	CMP R0, R1
	;if R0 > R1
		SUBGT R0, R1
	;if R0 < R1
		SUBLT R1, R0
	BLNE gcd
	
	POP{R1, PC}


Prompt			DCB	"Hello! Please enter in a number to be tested against x(20) and y(50): ", 0
LargestResult	DCB	"Your number is the largest.\n", 0
SmallestResult	DCB	"Your number is the smallest.\n", 0
BetweenResult	DCB	"Your number is inbetween 20 and 50.\n", 0
ProbeEQResult	DCB	"Your number is: ", 0
EqualResult		DCB "\nWhich is either 20 or 50\n", 0
Prompt2			DCB "\n\nNow enter a new number to be compared with your last entered number: ", 0
GuessPrompt		DCB	"\nOld Number is either less than or equal to New Number... GUESS WHICH ONE (: (1=LT, 2=EQ): ", 0
NoGuessPrompt	DCB	"\nOld Number is niether less than new number nor equal to new number\n", 0
Warning			DCB "\n\nWARNING THIS SHOULDN'T HAPPEN!\n\n", 0
Correct			DCB "CORRECT!\n", 0
Incorrect		DCB	"Incorrect!\n", 0
Goodbye			DCB "\nThank you for playing :)\n", 0
x		DCD 20 ; some predefined values to be used
y		DCD	50

	ALIGN
		
; The following data items are in the DATA area,
; so they are all READWRITE (i.e. can be modified at run-time),
; but are automatically initialized at assembly-time to zeroes 

	AREA    MyData, DATA, READWRITE
		
	ALIGN
	
z		SPACE	4 ; 4 bytes for our number
k		SPACE	4
Guess	SPACE 4
ArrayOfValues	DCD	24
		
;-------------------- END OF MODIFIABLE CODE ----------------------

	ALIGN

	END			; end of source program in this file