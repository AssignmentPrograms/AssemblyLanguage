; File: HA9-main.s
; Author: Brandon Jones

; This file needs to be in a Keil version 5 project, together with file init.s

; This is an initial demo program for HA4, which you need to change to complete HA4

; All future CS 238 Home Assignments, will have similar but different files,
; like HA5-main.s, HA6-main.s, etc.

; Executable code in HAx-main.s files should start at label main

	EXPORT	main		; this line is needed to interface with init.s
	

; Usable utility functions defined in file init.s
; Importing any label from another source file is necessary
; in order to use that label in this source file

	IMPORT	GetCh		; Input one ASCII character from the UART #1 window (from keyboard)
	IMPORT	PutCh		; Output one ASCII character to the UART #1 window
	IMPORT	PutCRLF		; Output CR and LF to the UART #1 window
    IMPORT	UDivMod		; Perform unsigned division to obtain quotient and remainder
	IMPORT	GetDec		; Input a signed number from the UART #1 window
	IMPORT	PutDec		; Output a signed number to the UART #1 window
	IMPORT	GetStr		; Input a CR-terminated ASCII string from the UART #1 window
	IMPORT	PutStr		; Output null-terminated ASCII string to the UART #1 window
	;IMPORT	FindGCD		; Compute the Greatest Common Divisor for two integers
	;IMPORT	PutBin		; Take a Decimal value and print the Binary equilivant
	;IMPORT gcd6

	AREA    MyCode, CODE, READONLY

	ALIGN			; highly recommended to start and end any area with ALIGN

; Start of executable code is at following label: main

main

;-------------------- START OF MODIFIABLE CODE ----------------------

	PUSH	{LR}		; save return address of caller in init.s
	
	MOV R0, #10
	MOV R1, #20
	MOV R2, #30
	MOV R3, #40
	BL MySub
	BL PutCRLF
	BL PutCRLF
	BL PutCRLF
	BL PutCRLF
	BL PutDec
	
	POP {PC}


MySub
	PUSH{R1, R2, LR}
	SUB R1, R1
	ADD R2, R1, #32
MyLoop
	CMP R0, #0
	ADDLT R1, #1
	MOV R0, R0, LSL #1
	SUBS R2, #1
	BHI MyLoop
	MOV R0, R1
	POP{R1,R2,PC}


;----------------------GCD-----------------------
; Compute the GCD for two input values
; Registers: R0 is register containing Value1 (a) . R1 is register containing Value2 (b).
; Output: In R0, contains the GCD of the two numbers
gcd
	PUSH	{LR, R1} ; Preserve all "scratchpad" registers except for the return register (R0)
				 ; Also preserve the current PC to be later
							
	; GCD algoritm
	; if ( a > b )
	CMP R0, R1
	BLS NoFirstThen
		; then
		SUB R0, R1 ; a = a - b
		BL gcd
		POP {PC, R1}
NoFirstThen
	; if ( a < b)
	CMP R0, R1
	BHS NoSecondThen
		;then
		SUB R1, R0
		BL gcd ; Recursively call this function
		POP {PC, R1}
NoSecondThen
	
	POP		{PC, R1} ; Restore the registers as they were and restore the PC value
	



; Some commonly used ASCII codes

CR	EQU	0x0D	; Carriage Return (to move cursor to beginning of current line)
LF	EQU	0x0A	; Line Feed (to move cursor to same column of next line)

; The following data items are in the CODE area,
; so they are all READONLY (i.e. cannot be modified at run-time),
; but they can be initialized at assembly-time to any value

	ALIGN
		
; The following data items are in the DATA area,
; so they are all READWRITE (i.e. can be modified at run-time),
; but are automatically initialized at assembly-time to zeroes 

	AREA    MyData, DATA, READWRITE
		
	ALIGN


;-------------------- END OF MODIFIABLE CODE ----------------------

	ALIGN

	END			; end of source program in this file