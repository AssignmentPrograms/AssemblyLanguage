int gcd6(int a, int b, int c, int d, int e, int f)
{
    int x = gcd(a, gcd(b, c)), y = gcd(gcd(d, e), f);
    return gcd(x, y);
}
